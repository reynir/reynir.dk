---
title: Root of Reynir
head_extra: '<script async src="./js/index.js" type="text/javascript"></script>'
---
<img src="./images/mario.jpg" id="mario" alt="Mario" />

<!-- Lambda lists: cons -->
<p>λxλyλz.z x y</p>

<!-- fst: λxλy.x -->
<!-- snd: λxλy.y -->

## Hello, World!

I've reproduced a list of recent posts here for your reading pleasure:

## Recent posts
{% for article in articles %}
- [{{article.title}}]({{article.location}}) - {{article.date.month_repr}} {{article.date.day}}, {{article.date.year}}
{% endfor %}
