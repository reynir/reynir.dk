---
title: About me
---
## About

I have an interest in programming languages, and in particular *functional* ones with a *good* type system.

I studied [datalogi (computer science)](http://cs.au.dk/) at [Aarhus University](https://au.dk/).

I have a [GitHub account](https://github.com/reynir) with various [projects](https://github.com/reynir?tab=repositories).
