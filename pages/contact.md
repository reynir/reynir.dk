---
title: Contact
---
## Email

I have written my email in [BNF](http://en.wikipedia.org/wiki/Backus%E2%80%93Naur_Form):

```
my_email  ::= user "@" domain
user      ::= "reynir"
domain    ::= subdomain "." TLD
subdomain ::= "reynir"
TLD       ::= "dk"
```

People say you shouldn’t put your email in plaintext on websites, because crawlers will find it and send you *SPAM* - and no one likes that!
