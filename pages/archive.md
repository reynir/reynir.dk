---
title: Blog
---
Here you can find all my previous posts:

{% for article in articles %}
- [{{article.title}}]({{article.location}}) - {{article.date.month_repr}} {{article.date.day}}, {{article.date.year}}
{% endfor %}
