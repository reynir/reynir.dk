---
date: 2014-10-05
title: DigitalOcean tcpdump?
description: I noticed an odd "tcpdump" event in DigitalOcean API output
---
Lately I have been working on implementing v2 of DigitalOcean's API.
While doing that I ran a query to get all actions on my account.
I discovered that the result by default is *paginated* with 25 results on each *page*.

I re-ran the query but asking for up to 1000 results.
While inspecting the result to see if I did get all 87 results this time I discovered this odd-looking action:

    {
      "id":XXX,
      "status":"completed",
      "type":"tcpdump",
      "started_at":"2014-04-28T08:26:51Z",
      "completed_at":"2014-04-28T08:28:00Z",
      "resource_id":YYY,
      "resource_type":"droplet",
      "region":"ams1"
    }

The *type* field has the value *tcpdump*.
To my knowledge I have never initiated a tcpdump on any of my droplets, and I didn't think it was possible.
In fact, I haven't been able to verify that it is or has been possible at all!

I'm still happy with DigitalOcean, although this looks a bit suspicious - I have no idea why they would run a tcpdump on my droplet.
