let caller = Filename.basename Sys.argv.(0)
let version = "%%VERSION%%"
let default_port = 8888

let watch port =
  let level = `Info in
  let module Dest = Blog.Make(struct
      let source = Yocaml.Path.rel []
    end)
  in
  let host = Fmt.str "http://localhost:%d" port in
  Yocaml_unix.serve ~level ~target:Dest.target ~port
  @@ fun () -> Dest.process_all ~host

let build () =
  let level = `Info in
  let module Dest = Blog.Make(struct
      let source = Yocaml.Path.rel []
    end)
  in
  let host = Fmt.str "/" in
  Yocaml_unix.run ~level
  @@ fun () -> Dest.process_all ~host

let man =
  let open Cmdliner in
  [ `S Manpage.s_authors; `P "reynir.dk" ]

let watch_cmd =
  let open Cmdliner in
  let doc =
    "Serve from the specified directory as an HTTP server and rebuild \
     website on demand"
  in
  let exits = Cmd.Exit.defaults in
  let port_arg =
    let doc = "The port" in
    let arg = Arg.info ~doc [ "port"; "P"; "p" ] in
    Arg.(value & opt int default_port & arg)
  in
  let info = Cmd.info "watch" ~version ~doc ~exits ~man in
  Cmd.v info Term.(const watch $ port_arg)


let build_cmd =
  let open Cmdliner in
  let doc = "Build the website into the specified directory" in
  let exits = Cmd.Exit.defaults in
  let info = Cmd.info "build" ~version ~doc ~exits ~man in
  Cmd.v info Term.(const build $ const ())

let cmd = 
  let open Cmdliner in
  let sdocs = Manpage.s_common_options in
  let doc = "Build, push or serve reynir.dk" in
  let default_info = Cmd.info caller ~version ~doc ~sdocs ~man in
  let default = Term.(ret (const (`Help (`Pager, None)))) in
  Cmd.group ~default default_info [ build_cmd; watch_cmd ]

let () = exit @@ Cmdliner.Cmd.eval cmd
